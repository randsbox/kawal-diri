from azure.functions import HttpRequest, HttpResponse
import logging


def main(req: HttpRequest) -> HttpResponse:
    data = req.get_json()
    events = data["events"]
    logging.info(events)
    return HttpResponse(body="OK", status_code=200)
